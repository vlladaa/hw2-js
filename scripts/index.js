// -----вопросы по теории-----

/*
1) типы данных js: Number (число), String (строка), Boolean (булевый/логический тип), Undefined (неопределенный тип),
   Null (нулевой тип), Object (объект), Symbol (символ) и, насколько я понимаю, BigInt (большое число)

2) оператор === сравнивает типы данных и значения операндов, а == только их значения

3) оператор это специальный символ, который используется для различных операций (другими словами - действий) над переменными или же иными значениями 

*/


let name = "";
do { name = prompt("what's your name?", `${name}`); }
while (name === null || name === "" || !(isNaN(name)))

let age = "";
do { age = prompt("How old are you?", `${age}`); }
while (age === null || age === "" || isNaN(age))

if (age < 18) {
alert("You are not allowed to visit this website");
}
else if (age >= 18 && age <= 22) {
if (confirm("Are you sure you want to continue?")) {
alert(`Welcome, ${name}`)
} else {
alert("You are not allowed to visit this website")
}
} else {
    alert(`Welcome, ${name}`)
};
